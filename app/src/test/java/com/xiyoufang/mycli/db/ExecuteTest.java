package com.xiyoufang.mycli.db;

import com.alibaba.druid.DbType;
import com.alibaba.druid.sql.SQLUtils;
import com.alibaba.druid.sql.ast.SQLStatement;
import com.alibaba.druid.sql.ast.statement.SQLDeleteStatement;
import com.alibaba.druid.sql.ast.statement.SQLSelectStatement;
import com.alibaba.druid.sql.parser.ParserException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;

/**
 * Created by 席有芳 on 2021/11/24.
 *
 * @author 席有芳
 */
class ExecuteTest {

    @Test
    void sqlParser() {
        List<SQLStatement> statements = SQLUtils.parseStatements("SELECT 1", DbType.mysql);
        Assertions.assertNotNull(statements);
        Assertions.assertFalse(statements.isEmpty());
        Assertions.assertTrue(statements.get(0) instanceof SQLSelectStatement);
        Assertions.assertThrows(ParserException.class, () -> SQLUtils.parseStatements("DELETE 1", DbType.mysql));
        statements = SQLUtils.parseStatements("DELETE FROM users", DbType.mysql);
        Assertions.assertTrue(statements.get(0) instanceof SQLDeleteStatement);
    }
}
