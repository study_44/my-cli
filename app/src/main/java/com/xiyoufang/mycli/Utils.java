package com.xiyoufang.mycli;

import com.jfinal.kit.StrKit;

import java.io.File;
import java.text.MessageFormat;
import java.util.Random;

/**
 * Created by 席有芳 on 2021/11/26.
 *
 * @author 席有芳
 */
public class Utils {

    private static final String TOKEN_STRING = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    /**
     * 获取环境变量文件
     *
     * @param fileName fileName
     * @param name     name
     * @return envFile
     */
    public static File getEnv(String fileName, String name) {
        return new File(System.getProperty("user.home", System.getenv("HOME")), MessageFormat.format(fileName, StrKit.notBlank(name) ? name + "." : ""));
    }

    /**
     * 休眠
     *
     * @param millis millis
     */
    public static void sleep(long millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    /**
     * 创建随机token
     *
     * @param length length
     * @return token
     */
    public static String token(int length) {
        StringBuilder token = new StringBuilder();
        int max = TOKEN_STRING.length() - 1;
        for (int i = 0; i < length; i++) {
            token.append(TOKEN_STRING.toCharArray()[new Random().nextInt(max)]);
        }
        return token.toString();
    }
}
