package com.xiyoufang.mycli;

import com.xiyoufang.mycli.db.*;
import com.xiyoufang.mycli.nginx.Nginx;
import com.xiyoufang.mycli.nginx.Proxy;
import com.xiyoufang.mycli.tunnel.L;
import com.xiyoufang.mycli.tunnel.R;
import com.xiyoufang.mycli.tunnel.Tunnel;
import picocli.CommandLine;

import java.util.concurrent.Callable;


/**
 * Created by 席有芳 on 2021/11/22.
 * 我的命令行工具
 * 将自己的一些最佳实践集成到命令行
 *
 * @author 席有芳
 */
@CommandLine.Command(name = "my-cli",
        version = {
                "My-cli v1.0.0",
                "Build 2021-11-27 (java version \"1.8.0_181\")",
                "(c) 2021 youfangxi@gmail.com",
                "Website: https://www.xiyoufang.com"
        },
        description = "提供命令行工具访问我的 (youfangxi@gmail.com) 最佳实践.",
        mixinStandardHelpOptions = true)
public class App implements Callable<Integer> {

    @Override
    public Integer call() {
        System.out.println("使用 -h 查看帮助信息");
        return 0;
    }

    public static void main(String[] args) {
        if (args == null || args.length == 0) {
            args = new String[]{"-V"};
        }
        int exitCode = new CommandLine(new App())
                .addSubcommand(
                        new CommandLine(new Db())
                                .addSubcommand(new com.xiyoufang.mycli.db.Init())
                                .addSubcommand(new Migrate())
                                .addSubcommand(new Create())
                                .addSubcommand(new Drop())
                                .addSubcommand(new Execute())
                )
                .addSubcommand(
                        new CommandLine(new Tunnel())
                                .addSubcommand(new com.xiyoufang.mycli.tunnel.Init())
                                .addSubcommand(new L())
                                .addSubcommand(new R())
                )
                .addSubcommand(
                        new CommandLine(new Nginx())
                                .addSubcommand(new Proxy())
                )
                .execute(args);
        System.exit(exitCode);
    }
}
