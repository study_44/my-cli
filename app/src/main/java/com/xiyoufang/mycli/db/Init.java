package com.xiyoufang.mycli.db;

import com.jfinal.kit.StrKit;
import com.jfinal.template.Engine;
import com.xiyoufang.mycli.Config;
import org.apache.commons.io.FileUtils;
import picocli.CommandLine;

import java.io.File;
import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Callable;

/**
 * Created by 席有芳 on 2021/11/23.
 *
 * @author 席有芳
 */
@CommandLine.Command(name = "init", description = "数据库工具: 初始化")
public class Init implements Callable<Integer> {
    /**
     * 环境变量文件
     */
    static final String ENV = ".my-cli/.{0}db.env";

    /**
     * 帮助指令
     */
    @CommandLine.Option(names = {"-h", "--help"}, description = "帮助信息", usageHelp = true)
    private boolean help;
    /**
     * 名称
     */
    @CommandLine.Option(names = {"-n", "--name"}, defaultValue = "", description = "配置名称: 对应一个数据库, 例如: mysql", interactive = true, echo = true)
    private String name;
    /**
     * JDBC连接符
     */
    @CommandLine.Option(names = {"-H", "--host"}, defaultValue = "jdbc:mysql://127.0.0.1:3306/test?serverTimezone=UTC", description = "连接符, 例如: jdbc:mysql://127.0.0.1:3306/test?serverTimezone=UTC", interactive = true, echo = true, required = true)
    private String url;
    /**
     * 用户名
     */
    @CommandLine.Option(names = {"-u", "--username"}, defaultValue = "root", description = "用户名, 例如: root", interactive = true, echo = true, required = true)
    private String username;
    /**
     * 密码
     */
    @CommandLine.Option(names = {"-p", "--password"}, defaultValue = "", description = "密码", interactive = true, echo = true)
    private String password;
    /**
     * migrate记录表
     */
    @CommandLine.Option(names = {"-t", "--table"}, defaultValue = "migrate_schema", description = "migrate记录表, 例如: migrate_schema", interactive = true, echo = true)
    private String table;
    /**
     * migrate脚本位置
     */
    @CommandLine.Option(names = {"-d", "--dir"}, defaultValue = "db/migrate", description = "migrate脚本位置, 例如: db/migrate", interactive = true, echo = true)
    private String location;
    /**
     * 配置模板
     */
    private static final String DB_ENV_TEMPLATE = "db.url=#(url)\n" +
            "db.username=#(username)\n" +
            "db.password=#(password)\n" +
            "migrate.table=#(table)\n" +
            "migrate.location=#(location)\n";

    @Override
    public Integer call() throws Exception {
        File envFile = new File(System.getProperty("user.home", System.getenv("HOME")), MessageFormat.format(ENV, StrKit.notBlank(name) ? name + "." : ""));
        if (envFile.exists()) {
            System.out.println("<" + envFile.getAbsolutePath() + "> 文件已经存在, 请手动删除后再试.");
        } else {
            Map<String, String> param = new HashMap<>();
            param.put("url", this.url);
            param.put("username", this.username);
            param.put("password", this.password);
            param.put("table", this.table);
            param.put("location", this.location);
            String content = Engine.use().getTemplateByString(DB_ENV_TEMPLATE).renderToString(param);
            FileUtils.writeStringToFile(envFile, content, Config.DEFAULT_CHARSET);
            System.out.println("初始化完成, 生成配置文件<" + envFile.getAbsolutePath() + ">");
        }
        return 0;
    }
}
