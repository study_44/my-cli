package com.xiyoufang.mycli.db;

import picocli.CommandLine;

import java.util.concurrent.Callable;

/**
 * Created by 席有芳 on 2021/11/23.
 *
 * @author 席有芳
 */
@CommandLine.Command(name = "db", description = "数据库工具")
public class Db implements Callable<Integer> {

    @CommandLine.Option(names = {"-h", "--help"}, usageHelp = true)
    private boolean help;

    @Override
    public Integer call() {
        System.out.println("使用 db -h 查看帮助信息");
        return 0;
    }
}
