package com.xiyoufang.mycli.db;

import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Db;
import picocli.CommandLine;

/**
 * Created by 席有芳 on 2021/11/24.
 *
 * @author 席有芳
 */
@CommandLine.Command(name = "create", description = "数据库工具: create database")
public class Create extends Connected {

    /**
     * 数据库名称
     */
    @CommandLine.Option(names = {"-d", "--database"}, description = "数据库名称, 例如: test", interactive = true, echo = true)
    protected String database = "";
    /**
     * 创建数据库携带的参数
     */
    @CommandLine.Option(names = {"-a", "--additional"}, description = "附加信息, 例如: collate utf8_general_ci", interactive = true, echo = true)
    protected String additional = "";

    @Override
    protected Integer doCall() {
        if (StrKit.notBlank(database)) {
            Db.use().update("create database " + database + " " + additional);
            System.out.println("<" + database + ">数据库已经被创建");
        }
        return 0;
    }
}
