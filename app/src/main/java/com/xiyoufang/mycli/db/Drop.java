package com.xiyoufang.mycli.db;

import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Db;
import picocli.CommandLine;

/**
 * Created by 席有芳 on 2021/11/24.
 *
 * @author 席有芳
 */
@CommandLine.Command(name = "drop", description = "数据库工具: drop database/table")
public class Drop extends Connected {

    /**
     * 数据库名称
     */
    @CommandLine.Option(names = {"-d", "--database"}, description = "数据库名称, 例如: test", interactive = true, echo = true)
    protected String database = "";
    /**
     * 表名称
     */
    @CommandLine.Option(names = {"-t", "--table"}, description = "表名称, 例如: users", interactive = true, echo = true)
    protected String table = "";

    @Override
    protected Integer doCall() {
        if (StrKit.notBlank(database)) {
            Db.use().update("drop database " + database);
            System.out.println("<" + database + ">数据库已经被删除");
        }
        if (StrKit.notBlank(table)) {
            Db.use().update("drop table " + table);
            System.out.println("<" + table + ">表已经被删除");
        }
        return 0;
    }
}
