package com.xiyoufang.mycli.nginx;

import picocli.CommandLine;

import java.util.concurrent.Callable;

/**
 * Created by 席有芳 on 2021/11/27.
 *
 * @author 席有芳
 */
@CommandLine.Command(name = "nginx", description = "NGINX工具")
public class Nginx implements Callable<Integer> {

    @CommandLine.Option(names = {"-h", "--help"}, usageHelp = true)
    private boolean help;

    @Override
    public Integer call() throws Exception {
        System.out.println("使用 nginx -h 查看帮助信息");
        return 0;
    }
}
