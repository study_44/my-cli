package com.xiyoufang.mycli.tunnel;

import picocli.CommandLine;

/**
 * Created by 席有芳 on 2021/11/26.
 *
 * @author 席有芳
 */
@CommandLine.Command(name = "l", description = "本地转远程")
public class L extends Connected {

    @Override
    protected Integer doCall() throws Exception {
        try {
            for (int i = 0; i < Math.min(froms.size(), tos.size()); i++) {
                Integer from = froms.get(i);
                Integer to = tos.get(i);
                int j = session.setPortForwardingL(from, session.getHost(), to);
                if (j > 0) {
                    System.out.printf("已经将本地%d端口，转发到%s的%d端口\n", from, session.getHost(), to);
                }
            }
            waitDisconnect();
        } catch (Exception e) {
            System.err.println("端口转发失败");
            throw e;
        }
        return 0;
    }
}
