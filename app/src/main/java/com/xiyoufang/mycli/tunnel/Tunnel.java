package com.xiyoufang.mycli.tunnel;

import picocli.CommandLine;

import java.util.concurrent.Callable;

/**
 * Created by 席有芳 on 2021/11/26.
 *
 * @author 席有芳
 */
@CommandLine.Command(name = "tunnel", description = "隧道工具")
public class Tunnel implements Callable<Integer> {

    @CommandLine.Option(names = {"-h", "--help"}, usageHelp = true)
    private boolean help;

    @Override
    public Integer call() {
        System.out.println("使用 tunnel -h 查看帮助信息");
        return 0;
    }
}
