package com.xiyoufang.mycli.tunnel;

import picocli.CommandLine;

import java.util.Date;

/**
 * Created by 席有芳 on 2021/11/26.
 *
 * @author 席有芳
 */
@CommandLine.Command(name = "r", description = "远程转本地")
public class R extends Connected {

    /**
     * 主机IP
     */
    @CommandLine.Option(names = {"-H", "--host"}, defaultValue = "127.0.0.1", description = "主机IP, 例如: 127.0.0.1", interactive = true, echo = true, required = true)
    private String host;

    @Override
    protected Integer doCall() throws Exception {
        try {
            for (int i = 0; i < Math.min(froms.size(), tos.size()); i++) {
                Integer from = froms.get(i);
                Integer to = tos.get(i);
                session.setPortForwardingR(from, host, to);
                System.out.printf("已经将%s的%d端口，转发本地%d端口\n", session.getHost(), from, to);
            }
            waitDisconnect();
        } catch (Exception e) {
            System.err.println("端口转发失败");
            throw e;
        }
        return 0;
    }
}
