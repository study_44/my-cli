package com.xiyoufang.mycli.tunnel;

import com.jfinal.kit.StrKit;
import com.jfinal.template.Engine;
import com.xiyoufang.mycli.Config;
import org.apache.commons.io.FileUtils;
import picocli.CommandLine;

import java.io.File;
import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Callable;

/**
 * Created by 席有芳 on 2021/11/26.
 *
 * @author 席有芳
 */
@CommandLine.Command(name = "init", description = "隧道工具: 初始化")
public class Init implements Callable<Integer> {
    /**
     * 环境变量文件
     */
    public static final String ENV = ".my-cli/.{0}tunnel.env";
    /**
     * 帮助指令
     */
    @CommandLine.Option(names = {"-h", "--help"}, description = "帮助信息", usageHelp = true)
    private boolean help;
    /**
     * 名称
     */
    @CommandLine.Option(names = {"-n", "--name"}, defaultValue = "", description = "配置名称: 一个服务器对应一个, 例如: server1")
    private String name;
    /**
     * 主机IP
     */
    @CommandLine.Option(names = {"-H", "--host"}, description = "主机IP, 例如: 127.0.0.1", interactive = true, echo = true, required = true)
    private String host;
    /**
     * SSH端口
     */
    @CommandLine.Option(names = {"-P", "--port"}, defaultValue = "22", description = "SSH端口, 例如: 22", interactive = true, echo = true, required = true)
    private String port;
    /**
     * 用户名
     */
    @CommandLine.Option(names = {"-u", "--username"}, defaultValue = "root", description = "用户名, 例如: root", interactive = true, echo = true, required = true)
    private String username;
    /**
     * 私钥
     */
    @CommandLine.Option(names = {"-k", "--key"}, defaultValue = "", description = "私钥", interactive = true, echo = true)
    private String key;
    /**
     * 密码
     */
    @CommandLine.Option(names = {"-p", "--password"}, defaultValue = "", description = "密码", interactive = true, echo = true)
    private String password;

    /**
     * 目标端口
     */
    @CommandLine.Option(names = {"-t", "--timeout"}, defaultValue = "30", description = "超时时间秒, 例如: 30")
    private String timeout;

    /**
     * 配置模板
     */
    private static final String DB_ENV_TEMPLATE = "host=#(host)\n" +
            "port=#(port)\n" +
            "username=#(username)\n" +
            "key=#(key)\n" +
            "password=#(password)\n" +
            "timeout=#(timeout)\n";


    @Override
    public Integer call() throws Exception {
        File env = new File(System.getProperty("user.home", System.getenv("HOME")), MessageFormat.format(ENV, StrKit.notBlank(name) ? name + "." : ""));
        if (env.exists()) {
            System.out.println("<" + env.getAbsolutePath() + "> 文件已经存在, 请手动删除后再试.");
        } else {
            Map<String, String> param = new HashMap<>();
            param.put("host", host);
            param.put("port", port);
            param.put("username", username);
            param.put("key", key);
            param.put("password", password);
            param.put("timeout", timeout);
            String content = Engine.use().getTemplateByString(DB_ENV_TEMPLATE).renderToString(param);
            FileUtils.writeStringToFile(env, content, Config.DEFAULT_CHARSET);
            System.out.println("初始化完成, 生成配置文件<" + env.getAbsolutePath() + ">文件");
        }
        return 0;
    }
}
