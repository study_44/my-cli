package com.xiyoufang.mycli;

/**
 * Created by 席有芳 on 2021/11/22.
 *
 * @author 席有芳
 */
public class Config {
    /**
     * 默认的编码格式
     */
    public static final String DEFAULT_CHARSET = "UTF-8";
}
