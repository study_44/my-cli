buildscript {
    repositories {
        mavenCentral()
    }

    dependencies {
        classpath("org.apache.commons:commons-exec:1.3")
        classpath("commons-io:commons-io:2.8.0")
    }
}

plugins {
    application
    id("com.github.johnrengelman.shadow") version "7.1.0"
}

val myCli = "my-cli"

version = "v1.0.0"

application {
    mainClass.set("com.xiyoufang.mycli.App")
    applicationName = myCli
}

repositories {
    mavenCentral()
}

dependencies {
    testImplementation("org.junit.jupiter:junit-jupiter-api:5.6.0")
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine")
    implementation("com.alibaba:druid:1.2.8")
    implementation("com.alibaba:fastjson:1.2.78")
    implementation("com.jcraft:jsch:0.1.55")
    implementation("com.jfinal:jfinal:4.9.17")
    implementation("commons-io:commons-io:2.8.0")
    implementation("info.picocli:picocli:4.6.1")
    implementation("mysql:mysql-connector-java:8.0.16")
    implementation("org.apache.commons:commons-exec:1.3")
    implementation("org.flywaydb:flyway-core:5.2.4")
    implementation("org.slf4j:slf4j-log4j12:1.7.32")
    annotationProcessor("info.picocli:picocli-codegen:4.6.1")
}

tasks.test {
    useJUnitPlatform()
}

tasks.jar {
    archiveBaseName.set(myCli)
}

tasks.named<com.github.jengelman.gradle.plugins.shadow.tasks.ShadowJar>("shadowJar") {
    archiveBaseName.set(myCli)
    mergeServiceFiles()
}

tasks.withType<JavaCompile> {
    sourceCompatibility = JavaVersion.VERSION_1_8.toString()
    targetCompatibility = JavaVersion.VERSION_1_8.toString()
}

// 将编译后的可执行文件复制到系统
task("installTo") {
    doLast {
        println("复制命令行到系统bin目录")
        org.apache.commons.io.FileUtils.copyFileToDirectory(
            File(buildDir, "install/my-cli/bin/my-cli"),
            File("/usr/local/bin/")
        )
        println("复制lib库到系统lib目录")
        org.apache.commons.io.FileUtils.copyDirectory(
            File(buildDir, "install/my-cli/lib/"),
            File("/usr/local/lib/")
        )
        val test = org.apache.commons.exec.CommandLine("my-cli")
        test.addArgument("-V")
        val handler = org.apache.commons.exec.DefaultExecuteResultHandler()
        org.apache.commons.exec.DefaultExecutor().execute(test, handler)
        handler.waitFor(3 * 1000)
    }
}
