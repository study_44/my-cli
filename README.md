# My CLI v1.0.0

My CLI 是我在多年开发过程中的一套最佳实践集合, 提供以命令行的方式访问.

# My CLI v1.0.0 支持的命令

在指令后面加上 -h 查看帮助信息

```shell
my-cli -h # 帮助信息
my-cli -V  # 版本信息
```

## DB工具

一系列和数据库相关的命令

```shell
my-cli db -h
my-cli db init  # 初始化配置
my-cli db migrate # 数据库迁移
my-cli db drop  # 删除数据库或者表
my-cli db create # 创建数据库
my-cli db execute # 执行脚本
```

## Tunnel工具

隧道工具，用于端口转发

```shell
my-cli tunnel -h
my-cli tunnel init # 初始化配置
my-cli tunnel l # 本地转服务器
my-cli tunnel r # 服务器转本地
```

## Nginx工具

```shell
my-cli nginx -h
my-cli nginx proxy # 生成反向代理配置
```

# 怎么开发 My CLI ?

设置本地开发环境

```shell
git clone git@gitee.com:xiyoufang/my-cli.git
cd my-cli
./gradlew
```

关键的技术:

- https://picocli.info/
- https://gradle.org/
- https://docs.gradle.org/current/userguide/distribution_plugin.html
- https://docs.gradle.org/current/userguide/application_plugin.html
- https://imperceptiblethoughts.com/shadow/

使用Gradle Distribution Plugin构建非Fat-Jar （依赖的jar都单独在lib目录）:

```shell
./gradlew clean installDist
```

测试生成的可执行脚本（非Fat-jar模式）

```shell
app/build/install/my-cli/bin/my-cli -V
```

使用Gradle Shadow Plugin构建Fat-Jar（所有的jar合并到一个jar中）:

```shell
./gradlew clean installShadowDist
```

测试生成的可执行脚本（Fat-jar模式）

```shell
app/build/install/app-shadow/bin/my-cli -V
```

# 安装 My CLI

将命令行工具加入到系统路径

## Mac/Linux通过Gradle Task安装

```shell
./gradlew installTo
```

## 手动安装

```shell
# 查看安装的lib目录
ls -la /usr/local/lib/ | grep jar
```

### 非 Fat-Jar 模式

```shell
cp app/build/install/my-cli/bin/my-cli /usr/local/bin/my-cli
cp app/build/install/my-cli/lib/*.jar /usr/local/lib/
chmod u+x /usr/local/bin/my-cli
my-cli -h
```

例如: 将本地的8090和8091端口转发到名称为www的远程服务器8090和8091端口

```shell
my-cli tunnel l -f 8090 -f 8091 -t 8090 -t 8091 -n www
```

### Fat-Jar 模式

```shell
cp app/build/install/app-shadow/bin/my-cli /usr/local/bin/my-cli
cp app/build/install/app-shadow/lib/my-cli-v1.0.0-all.jar /usr/local/lib/my-cli-v1.0.0-all.jar
chmod u+x /usr/local/bin/my-cli
my-cli -h
```
